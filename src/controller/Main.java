package controller;

import java.util.ArrayList;

import model.BankAccount;
import model.Company;
import model.Country;
import model.Data;
import model.Measurable;
import model.Person;
import model.Product;
import model.TaxCalculator;
import model.Taxable;

public class Main {

	public static void main(String[] args) {
			
		//���ҧ����êش Person ������¡�����ʹ average 㹤��� Data class ���ͻ����żŵ���êش�ͧ Person ����ʴ������٧�����	
		Person[] height = new Person[] {
				new Person("A", 180),
				new Person("B", 156),
				new Person("C", 160),
				new Person("D", 154)
		};
		
		Data data = new Data();
		double avgHeight = data.average(height);
		System.out.println("1.Average Height="+avgHeight);
		
		
		//���ҧ�ͻਤ�ͧ BankAccount, Country ��� Person �ҡ��� ������¡�����ʹ min ��������ʴ������ŷ����·���ش�ͧ���Ф���
		BankAccount m1 = new BankAccount(12000);
		BankAccount m2 = new BankAccount(4000);
		
		Measurable minBalance = data.min(m1, m2);
		System.out.println("2.Min Balance = "+minBalance.getMeasure());
		
	
		Country c1 = new Country("Thailand", 513000, 70, 387);
		Country c2 = new Country("Lao", 236800, 7, 11.14);
		
		Measurable minArea = data.min(c1, c2);
		System.out.println("  Min Area = "+minArea.getMeasure());
		
		Person p1 = new Person("A", 180);
		Person p2 =	new Person("B", 156);
		
		Measurable minHeight = data.min(p1, p2);
		System.out.println("  Min Height = "+minHeight.getMeasure());
		
	
		//���ҧ ArrayList �ͧ Person, Company, Product ���� ArrayList ���Ẻ�¡������ ���Ẻ�շ�� 3 �������������� ArrayList ���ǡѹ����
		ArrayList<Taxable> listPerson = new ArrayList<Taxable>();
		Person ps1 =new Person("Female","A", 100000);
		Person ps2 =new Person("Male","B", 500000);
		Person ps3 =new Person("Female","C", 300000);
		listPerson.add(ps1);
		listPerson.add(ps2);
		listPerson.add(ps3);
		
	
		TaxCalculator t = new TaxCalculator();
		System.out.println("3.Sum Tax Person = "+t.sum(listPerson));
		
		
		ArrayList<Taxable> listCompany = new ArrayList<Taxable>();
		Company cp1 =new Company("Food","A", 1000000,800000);
		Company cp2 =new Company("Computer","B", 1500000,500000);
		Company cp3 =new Company("Book","C", 2000000,1000000);
		listCompany.add(cp1);
		listCompany.add(cp2);
		listCompany.add(cp3);
		
	
		TaxCalculator t2 = new TaxCalculator();
		System.out.println("  Sum Tax Company = "+t2.sum(listCompany));
		
		
		ArrayList<Taxable> listProduct = new ArrayList<Taxable>();
		Product pr1 =new Product("Food","A", 100);
		Product pr2 =new  Product("Computer","B",800);
		Product pr3 =new  Product("Book","C", 1000);
		listProduct.add(pr1);
		listProduct.add(pr2);
		listProduct.add(pr3);
		
	
		TaxCalculator t3 = new TaxCalculator();
		System.out.println("  Sum Tax Product = "+t3.sum(listProduct));
		
		ArrayList<Taxable> list = new ArrayList<Taxable>();
		Company cp4 =new Company("Food","A", 1000000,800000);
		Person ps4 =new Person("Female","B", 100000);
		Product pr4 =new  Product("Food","C", 100);
		list.add(cp4);
		list.add(ps4);
		list.add(pr4);
		
	
		TaxCalculator t4 = new TaxCalculator();
		System.out.println("  Sum Tax = "+t4.sum(list));
		
	}

}
