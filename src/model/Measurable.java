package model;

public interface Measurable {
	double getMeasure();
}
