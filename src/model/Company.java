package model;

public class Company implements Taxable {
	private String information, nameCompany;
	private double income, expenses;
	
	public Company(String information,String nameCompany,double income,double expenses){
		this.information = information;
		this.nameCompany = nameCompany;
		this.income = income;
		this.expenses = expenses;
		
	}
	public String getInformation(){
		return information;
	}
	
	public String getNameCompany(){
		return nameCompany;
	}
	
	public double getIncome(){
		return income;
	}
	
	public double getExpenses(){
		return expenses;
	}
	
	@Override
	public double getTax() {
		// �������� 30% �ҡ���èҡ��áԨ �� ����Ѻ�� 1,000,000 �ҷ ��¨����� 800,000 �ҷ �ѧ��� ������ 200,000 ������ͧ�������� 60,000 �ҷ
		return ((0.3)*(income-expenses));
	}

}
