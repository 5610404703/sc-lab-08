package model;

public class BankAccount implements Measurable, Comparable<BankAccount> {

	private double balance; 

	public BankAccount(double initialBalance) {  
		balance = initialBalance;
	}

	public void deposit(double amount) {  
		balance = balance + amount;
	}

	public void withdraw(double amount) {  
		if (balance < amount) 
			throw new IllegalArgumentException();
		balance = balance - amount;
	}

	public double getBalance() {  
		return balance; 
	}

	@Override
	public double getMeasure() {
		return balance;
	}

	@Override
	public int compareTo(BankAccount other) {
		if (this.balance < other.balance ) { return -1; }
		if (this.balance > other.balance ) { return 1;  }
		return 0;
	}
}
