package model;

import java.security.spec.ECFieldFp;

public class Person implements Measurable, Taxable{
	private String name, information;
	private double height, emolument;
	
	
	public Person(String name, double height){
		this.name = name;
		this.height = height;
	}
	
	public Person(String information, String name, double emolument){
		this.information = information;
		this.name = name;
		this.emolument = emolument;
	}
	
	public String getName(){
		return name;
	}
	
	public String getInformation(){
		return information;
	}
	
	public double getHeight(){
		return height;
	}
	
	public double getEmolument(){
		return emolument;
	}
	
	@Override
	public double getMeasure() {
		return height;
	}
	
	@Override
	public double getTax() {
		// Person (�ؤ�Ÿ�����): ��������Ẻ��鹺ѹ� ����Ǥ�� 300,000 �ҷ�á�е�ͧ���� 5% ��� 300,001 ���仨Ш��� 10% 
		//�� ���������ͻ��� 100,000 �ҷ �Ш������� 5%=5,000 �ҷ ����������ͻ��� 500,000 �ҷ���� ��ǹ 300,000 �ҷ�á
		//�Ш��� 5%=15,000 �ҷ ��з������� 200,000 �ҷ�Ш��� 10%=20,000 �ҷ �ѧ��� �����������Թ (15,000+20,000=35,000 �ҷ)
		if(this.emolument>=0 && this.emolument<= 300000){
			
			return (0.05)*this.emolument;
		}
		return (0.05)*300000 + (0.1)*(this.emolument-300000);
		
		
	}
}