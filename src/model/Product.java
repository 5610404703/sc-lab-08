package model;

public class Product implements Taxable {
	private String information, nameProduct;
	private double price;
	public Product(String information,String nameProduct,double price){
		this.information = information;
		this.nameProduct = nameProduct;
		this.price = price;
		
	}
	
	public String getInformation(){
		return information;
	}
	
	public String getNameProduct(){
		return nameProduct;
	}
	
	public double price(){
		return price;
	}
	
	@Override
	public double getTax() {
		// �������� 7% �ҡ�Ҥ��Թ��� �� �Թ����Ҥ� 100 �ҷ �������� 7 �ҷ 
		return (0.07)*price;
	}
}
